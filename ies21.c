/* Processed by ecpg (15.1) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "D:\\descarregues\\ies21.sql"
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2021-03-20 22:18:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'WIN1252';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 134145)
-- Name: escola; Type: SCHEMA; Schema: -; Owner: damvi
--
DROP DATABASE ies21;
--
-- Name: ies21; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE ies21 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Catalan_Spain.1252' LC_CTYPE = 'Catalan_Spain.1252';


ALTER DATABASE institut OWNER TO postgres;

\connect ies21

CREATE SCHEMA escola;


ALTER SCHEMA escola OWNER TO postgres;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 134146)
-- Name: alumnes; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.alumnes (
	idalumne int4 NOT NULL,
    nom character varying(15) NOT NULL,
    cognom1 character varying(15) NOT NULL,
    datanaixement date,
    grup integer NOT NULL,
    cognom2 character varying(15),
    actiu boolean DEFAULT true,
    repetidor boolean DEFAULT false
);

ALTER TABLE escola.alumnes OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 134151)
-- Name: qualificacions; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.qualificacions (
    id integer NOT NULL,
    idalumne integer,
    idunitatformativa integer,
    idprofessor integer,
    nota1c integer,
    nota2c integer
);


ALTER TABLE escola.qualificacions OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 134154)
-- Name: alumnesunitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.qualificacions ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.alumnesunitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 206 (class 1259 OID 134156)
-- Name: aules; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.aules (
    id integer NOT NULL,
    nomaula character varying(10),
    capacitat integer
);


ALTER TABLE escola.aules OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 134159)
-- Name: aules_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.aules ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.aules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 208 (class 1259 OID 134161)
-- Name: cicles; Type: TABLE; Schema: escola; Owner: damvi
--

CREATE TABLE escola.cicles (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    descripcio character varying(50),
    nivell character(1) DEFAULT 'S'::bpchar
);


ALTER TABLE escola.cicles OWNER TO damvi;

--
-- TOC entry 209 (class 1259 OID 134165)
-- Name: cicles_id_seq; Type: SEQUENCE; Schema: escola; Owner: damvi
--

ALTER TABLE escola.cicles ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.cicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 210 (class 1259 OID 134167)
-- Name: grups; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.grups (
    id integer NOT NULL,
    nom character varying(10) NOT NULL,
    tutor integer,
    cicle integer,
    curs integer
);


ALTER TABLE escola.grups OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 134170)
-- Name: grups_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.grups ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.grups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 212 (class 1259 OID 134172)
-- Name: grupsunitatsformatives; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.grupsunitatsformatives (
    id integer NOT NULL,
    idgrup integer,
    idunitatformativa integer,
    idprofessor integer,
    idaula integer
);


ALTER TABLE escola.grupsunitatsformatives OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 134175)
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.grupsunitatsformatives ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.grupsunitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 214 (class 1259 OID 134177)
-- Name: moduls; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.moduls (
    modul integer NOT NULL,
    hores integer NOT NULL,
    nom character varying(40) NOT NULL,
    cicle integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE escola.moduls OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 134180)
-- Name: moduls_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.moduls ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.moduls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 216 (class 1259 OID 134182)
-- Name: professor; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.professor (
    id integer NOT NULL,
    nom character varying(20) NOT NULL,
    cognom character varying(20)
);


ALTER TABLE escola.professor OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 134185)
-- Name: professor_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.professor ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME escola.professor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 218 (class 1259 OID 134187)
-- Name: unitatsformatives; Type: TABLE; Schema: escola; Owner: postgres
--

CREATE TABLE escola.unitatsformatives (
    id integer NOT NULL,
    idmodul integer,
    unitat integer,
    hores integer
);


ALTER TABLE escola.unitatsformatives OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 134190)
-- Name: unitatsformatives_id_seq; Type: SEQUENCE; Schema: escola; Owner: postgres
--

ALTER TABLE escola.unitatsformatives ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME escola.unitatsformatives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 2902 (class 0 OID 134146)
-- Dependencies: 203
-- Data for Name: alumnes; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.alumnes VALUES (1, 'XAVIER', 'BASTOS', '2000-06-22',  1, 'PASTOR', false, false);
INSERT INTO escola.alumnes VALUES (15, 'PABLO', 'CHUNGARA', '2000-03-28', 2, 'GIRON', true, false);
INSERT INTO escola.alumnes VALUES (5, 'MAX', 'ORTIN', '2001-12-28', 1, 'ARAGONES', false, false);
INSERT INTO escola.alumnes VALUES (18, 'IVAN', 'GARCIA', '1991-03-12', 1, 'VIDAL', true, false);
INSERT INTO escola.alumnes VALUES (19, 'DANIEL', 'GIMENEZ', '1995-08-25', 1, 'ARRABAL', true, false);
INSERT INTO escola.alumnes VALUES (20, 'ADRIAN', 'GINER', '2002-09-06', 1, 'LOPEZ', true, false);
INSERT INTO escola.alumnes VALUES (22, 'FRANCISCO', 'ALJAMA', '1998-12-08', 3, 'MANJON', true, false);
INSERT INTO escola.alumnes VALUES (23, 'ALBERT', 'BUENO', '1999-09-21', 3, 'ALVAREZ', true, false);
INSERT INTO escola.alumnes VALUES (14, 'ADRIAN', 'CARO', '2000-07-10', 2, 'MUDARRA', false, false);
INSERT INTO escola.alumnes VALUES (25, 'AARON', 'GAMARRA', '2001-04-09', 3, 'TORRES', false, false);
INSERT INTO escola.alumnes VALUES (2, 'DAVID', 'MARCHAN', '2001-05-08', 1, 'MARTIN', true, false);
INSERT INTO escola.alumnes VALUES (4, 'RUBEN', 'MENDEZ', '2003-02-03', 1, 'HERNANDEZ', true, false);
INSERT INTO escola.alumnes VALUES (7, 'SANTIAGO', 'PINEIRO', '2002-08-31', 1, 'YAGUES', true, false);
INSERT INTO escola.alumnes VALUES (8, 'IKER', 'QUIMI', '2002-11-12', 1, 'CALDERON', true, false);
INSERT INTO escola.alumnes VALUES (9, 'IVAN', 'SANCHEZ', '2002-11-16', 1, 'AMADOR', true, false);
INSERT INTO escola.alumnes VALUES (10, 'POL', 'SANMARTI', '2001-11-27', 1, 'CRUZ', true, false);
INSERT INTO escola.alumnes VALUES (11, 'MOISES', 'ZAMBRANO', '1998-03-25', 1, 'HERNANDEZ', true, false);
INSERT INTO escola.alumnes VALUES (12, 'ERIC', 'BARRIENTOS', '2000-08-25', 2, 'SALVADOR', true, false);
INSERT INTO escola.alumnes VALUES (24, 'ADRIA', 'DACOSTA', '1992-10-25', 3, 'SERRANO', true, false);
INSERT INTO escola.alumnes VALUES (26, 'ANGEL', 'GARCIA', '2002-02-16', 3, 'MOYA', true, false);
INSERT INTO escola.alumnes VALUES (27, 'ARNAU', 'GONZALEZ', '2002-07-26', 3, 'OLIVE', true, false);
INSERT INTO escola.alumnes VALUES (28, 'PAU', 'GUILLEN', '1999-12-19', 3, 'CARRANZA', true, false);
INSERT INTO escola.alumnes VALUES (29, 'DANIEL', 'LEIVA', '1995-04-13', 3, 'POZO', true, false);
INSERT INTO escola.alumnes VALUES (30, 'LUIS', 'MEDINA', '2000-02-29', 3, 'FERNANDEZ', true, false);
INSERT INTO escola.alumnes VALUES (31, 'VALENTINA', 'PENA', '1998-07-10', 3, 'MANGINI', true, false);
INSERT INTO escola.alumnes VALUES (32, 'JULI', 'RAGNI', '2002-03-26', 3, 'ALARI', true, false);
INSERT INTO escola.alumnes VALUES (34, 'MARTA', 'BRUSTENGA', '2001-10-08', 4, 'HITA', true, false);
INSERT INTO escola.alumnes VALUES (35, 'GERARD', 'LASTRES', '2001-02-15', 4, 'RUIZ', true, false);
INSERT INTO escola.alumnes VALUES (36, 'PAU', 'MARTINEZ', '2002-10-01', 4, 'GARCIA', true, false);
INSERT INTO escola.alumnes VALUES (37, 'FRANCESC', 'MEDINA', '2000-01-21', 4, 'GIMENEZ', true, false);
INSERT INTO escola.alumnes VALUES (38, 'SERGIO', 'MONFORT', '2002-01-03', 4, 'MOYA', true, false);
INSERT INTO escola.alumnes VALUES (39, 'ERIC', 'NOGUERA', '1998-02-03', 4, 'GALAN', true, false);
INSERT INTO escola.alumnes VALUES (41, 'CARMEN', 'PEREZ', '2000-01-26', 4, 'CONTRERAS', true, false);
INSERT INTO escola.alumnes VALUES (42, 'IKER', 'RAMIREZ', '1999-08-18', 4, 'UJAQUE', true, false);
INSERT INTO escola.alumnes VALUES (43, 'ALEX', 'RODRIGUEZ', '2001-05-22', 4, 'LORES', true, false);
INSERT INTO escola.alumnes VALUES (45, 'XAVIER', 'TIO', '1990-07-22', 4, 'RUIZ', true, false);
INSERT INTO escola.alumnes VALUES (46, 'RUBEN', 'URBANO', '1998-07-22', 4, 'MARTINEZ', true, false);
INSERT INTO escola.alumnes VALUES (47, 'ALBERT', 'VALIENTE', '1995-10-14', 4, 'JORGE', true, false);
INSERT INTO escola.alumnes VALUES (13, 'MARC', 'BATISTI', '1997-01-13', 2, 'MORENO', true, true);
INSERT INTO escola.alumnes VALUES (16, 'IVAN', 'ESPINOSA', '2000-01-28', 1, 'LEON', true, true);
INSERT INTO escola.alumnes VALUES (17, 'MARC', 'FERNANDEZ', '2000-04-20', 1, 'RAMOS', true, true);
INSERT INTO escola.alumnes VALUES (21, 'ALEJANDRO', 'MORILLO', '1999-03-10', 5, 'ROLDAN', true, true);
INSERT INTO escola.alumnes VALUES (3, 'JAVI', 'MAURI', '1984-12-20', 1, 'ULLATE', true, true);
INSERT INTO escola.alumnes VALUES (6, 'FELIX', 'PELLICER', '2001-03-19', 1, 'BAUTISTA', true, true);
INSERT INTO escola.alumnes VALUES (33, 'RICARDO', 'RAMOS', '1995-03-18', 3, 'TORREZ', true, true);
INSERT INTO escola.alumnes VALUES (40, 'RUBEN', 'PARRA', '2000-06-18', 4, 'GONZALEZ', true, true);
INSERT INTO escola.alumnes VALUES (44, 'RAUL', 'RUIZ', '2000-01-20', 4, 'BARBA', true, true);


--
-- TOC entry 2905 (class 0 OID 134156)
-- Dependencies: 206
-- Data for Name: aules; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (1, 'C2', 30);
INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (2, 'C3', 30);
INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (3, 'C4', 30);
INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (4, 'F3', 22);
INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (5, '1.6', 40);
INSERT INTO escola.aules OVERRIDING SYSTEM VALUE VALUES (6, 'A1', 20);


--
-- TOC entry 2907 (class 0 OID 134161)
-- Dependencies: 208
-- Data for Name: cicles; Type: TABLE DATA; Schema: escola; Owner: damvi
--

INSERT INTO escola.cicles OVERRIDING SYSTEM VALUE VALUES (1, 'ASIX', 'Administració de Sistemes', 'S');
INSERT INTO escola.cicles OVERRIDING SYSTEM VALUE VALUES (2, 'DAM ', 'Desenvolupament d''Aplicacions', 'S');
INSERT INTO escola.cicles OVERRIDING SYSTEM VALUE VALUES (3, 'DAMVI', 'Desenvolupament d''Aplicacions VideoJocs', 'S');
INSERT INTO escola.cicles OVERRIDING SYSTEM VALUE VALUES (4, 'SMX', 'Sistemes MicroInformàtics', 'M');


--
-- TOC entry 2909 (class 0 OID 134167)
-- Dependencies: 210
-- Data for Name: grups; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (1, 'ASIX1B', 3, 1, 1);
INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (2, 'DAM1B', 3, 2, 1);
INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (3, 'DAMVI1D', 8, 3, 1);
INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (4, 'DAMVI1E', 8, 3, 1);
INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (5, 'ASIX2', 1, 1, 2);
INSERT INTO escola.grups OVERRIDING SYSTEM VALUE VALUES (6, 'DAMVI2', 7, 3, 2);


--
-- TOC entry 2911 (class 0 OID 134172)
-- Dependencies: 212
-- Data for Name: grupsunitatsformatives; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (1, 1, 1, 2, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (2, 1, 2, 2, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (3, 1, 3, 2, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (4, 1, 4, 3, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (5, 1, 5, 3, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (6, 1, 6, 3, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (7, 1, 7, 1, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (8, 1, 8, 1, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (9, 1, 9, 1, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (10, 1, 10, 4, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (11, 1, 11, 4, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (12, 1, 12, 4, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (13, 1, 13, 5, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (14, 1, 14, 5, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (15, 1, 15, 5, 1);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (16, 1, 16, 6, 4);
INSERT INTO escola.grupsunitatsformatives OVERRIDING SYSTEM VALUE VALUES (17, 1, 17, 6, 4);


--
-- TOC entry 2913 (class 0 OID 134177)
-- Dependencies: 214
-- Data for Name: moduls; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (1, 231, 'Implantacio Sistemes Operatius', 1, 1);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (2, 165, 'Gestio de base de dades', 1, 2);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (3, 165, 'Programacio basica', 1, 3);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (4, 99, 'Llenguatge de Marques', 1, 4);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (5, 99, 'Fonaments de Maquinari', 1, 5);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (6, 132, 'Admin de Sistemes Operatius', 1, 6);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (10, 99, 'Administracio de SGBDs', 1, 7);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (9, 66, 'Implantacio d''Aplicacions WEB', 1, 8);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (7, 165, 'Planificacio i Admin Xarxes', 1, 9);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (8, 99, 'Serveis de Xarxa i Internet', 1, 12);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (12, 99, 'Formacio i Orientacio Laboral', 1, 10);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (11, 99, 'Seguretat i Alta Disponibilitat', 1, 13);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (13, 66, 'Empresa i Iniciativa Empresarial', 1, 14);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (14, 99, 'Projecte', 1, 15);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (15, 317, 'Formacio en centres de treball', 1, 16);
INSERT INTO escola.moduls OVERRIDING SYSTEM VALUE VALUES (2, 138, 'Base de Dades', 4, 31);


--
-- TOC entry 2915 (class 0 OID 134182)
-- Dependencies: 216
-- Data for Name: professor; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.professor VALUES (1, 'Gemma', 'Raimat');
INSERT INTO escola.professor VALUES (2, 'Ivan', 'Baranda');
INSERT INTO escola.professor VALUES (3, 'Gregorio', 'Santamaria');
INSERT INTO escola.professor VALUES (4, 'Isabel', 'de Andrés');
INSERT INTO escola.professor VALUES (5, 'Lino', 'de la Muñoza');
INSERT INTO escola.professor VALUES (6, 'Rosa', 'Vives');
INSERT INTO escola.professor VALUES (7, 'Marc', 'Albareda');
INSERT INTO escola.professor VALUES (8, 'Hector', 'Mudarra');
INSERT INTO escola.professor VALUES (9, 'Daniel', 'Fernandez');
INSERT INTO escola.professor VALUES (10, 'Eloi', 'Vazquez');
INSERT INTO escola.professor VALUES (11, 'Sausan', 'Battikh');


--
-- TOC entry 2903 (class 0 OID 134151)
-- Dependencies: 204
-- Data for Name: qualificacions; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (1, 2, 4, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (2, 2, 5, 3, 8, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (3, 1, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (4, 3, 5, 3, 7, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (5, 4, 4, 3, 4, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (6, 4, 5, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (7, 5, 4, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (8, 5, 5, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (9, 6, 4, 3, 9, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (10, 6, 5, 3, 9, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (11, 7, 4, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (12, 7, 5, 3, 4, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (13, 8, 5, 3, 7, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (14, 9, 4, 3, 4, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (15, 9, 5, 3, 7, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (16, 10, 4, 3, 7, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (17, 10, 5, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (18, 11, 4, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (19, 11, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (20, 12, 4, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (21, 12, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (22, 13, 4, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (23, 13, 5, 3, 8, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (24, 14, 4, 3, 9, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (25, 14, 5, 3, 8, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (26, 15, 4, 3, 8, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (27, 15, 5, 3, 9, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (28, 16, 4, 3, 4, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (29, 16, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (30, 17, 4, 3, 7, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (31, 17, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (32, 18, 5, 3, 0, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (33, 19, 5, 3, 4, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (34, 23, 4, 3, 5, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (35, 23, 5, 3, 6, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (52, 17, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (36, 1, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (37, 2, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (38, 3, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (39, 4, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (40, 5, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (41, 6, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (42, 7, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (43, 8, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (44, 9, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (45, 10, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (46, 11, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (47, 12, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (48, 13, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (49, 14, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (50, 15, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (51, 16, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (53, 18, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (54, 19, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (55, 20, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (56, 21, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (57, 22, 6, 3, NULL, NULL);
INSERT INTO escola.qualificacions OVERRIDING SYSTEM VALUE VALUES (58, 23, 6, 3, NULL, NULL);


--
-- TOC entry 2917 (class 0 OID 134187)
-- Dependencies: 218
-- Data for Name: unitatsformatives; Type: TABLE DATA; Schema: escola; Owner: postgres
--

INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (10, 4, 1, 45);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (11, 4, 2, 27);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (12, 4, 3, 27);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (13, 5, 1, 44);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (1, 1, 1, 72);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (2, 1, 2, 90);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (3, 1, 3, 36);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (4, 2, 1, 44);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (5, 2, 2, 88);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (7, 3, 1, 85);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (8, 3, 2, 50);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (9, 3, 3, 30);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (16, 9, 1, 44);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (17, 9, 2, 44);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (6, 7, 1, 66);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (14, 10, 1, 66);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (15, 10, 2, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (18, 1, 4, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (19, 5, 2, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (20, 6, 1, 99);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (21, 6, 2, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (22, 12, 1, 25);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (23, 12, 2, 25);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (24, 12, 3, 25);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (25, 12, 4, 24);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (26, 5, 3, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (27, 9, 3, 66);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (28, 13, 1, 24);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (29, 13, 2, 24);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (30, 13, 3, 27);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (31, 13, 4, 24);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (32, 8, 1, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (33, 8, 2, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (34, 2, 3, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (35, 7, 3, 33);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (36, 14, 1, 66);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (37, 15, 1, 99);
INSERT INTO escola.unitatsformatives OVERRIDING SYSTEM VALUE VALUES (38, 16, 1, 317);


--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 205
-- Name: alumnesunitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.alumnesunitatsformatives_id_seq', 58, true);


--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 207
-- Name: aules_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.aules_id_seq', 6, true);


--
-- TOC entry 2927 (class 0 OID 0)
-- Dependencies: 209
-- Name: cicles_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: damvi
--

SELECT pg_catalog.setval('escola.cicles_id_seq', 4, true);


--
-- TOC entry 2928 (class 0 OID 0)
-- Dependencies: 211
-- Name: grups_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.grups_id_seq', 6, true);


--
-- TOC entry 2929 (class 0 OID 0)
-- Dependencies: 213
-- Name: grupsunitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.grupsunitatsformatives_id_seq', 17, true);


--
-- TOC entry 2930 (class 0 OID 0)
-- Dependencies: 215
-- Name: moduls_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.moduls_id_seq', 31, true);


--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 217
-- Name: professor_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.professor_id_seq', 1, true);


--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 219
-- Name: unitatsformatives_id_seq; Type: SEQUENCE SET; Schema: escola; Owner: postgres
--

SELECT pg_catalog.setval('escola.unitatsformatives_id_seq', 20, true);


--
-- TOC entry 2745 (class 2606 OID 134193)
-- Name: alumnes alumnes_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.alumnes
    ADD CONSTRAINT alumnes_pk PRIMARY KEY (idalumne);


--
-- TOC entry 2747 (class 2606 OID 134195)
-- Name: qualificacions alumnesunitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_pk PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 134197)
-- Name: aules aules_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.aules
    ADD CONSTRAINT aules_pk PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 134199)
-- Name: cicles cicles_pk; Type: CONSTRAINT; Schema: escola; Owner: damvi
--

ALTER TABLE ONLY escola.cicles
    ADD CONSTRAINT cicles_pk PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 134201)
-- Name: grups grups_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_pk PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 134203)
-- Name: grupsunitatsformatives grupsunitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_pk PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 134205)
-- Name: moduls moduls_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_pk PRIMARY KEY (id);


--
-- TOC entry 2759 (class 2606 OID 134207)
-- Name: moduls moduls_un; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_un UNIQUE (cicle, modul);


--
-- TOC entry 2761 (class 2606 OID 134209)
-- Name: professor professor_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.professor
    ADD CONSTRAINT professor_pk PRIMARY KEY (id);


--
-- TOC entry 2763 (class 2606 OID 134211)
-- Name: unitatsformatives unitatsformatives_pk; Type: CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.unitatsformatives
    ADD CONSTRAINT unitatsformatives_pk PRIMARY KEY (id);


--
-- TOC entry 2764 (class 2606 OID 134212)
-- Name: alumnes alumnes_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.alumnes
    ADD CONSTRAINT alumnes_fk FOREIGN KEY (grup) REFERENCES escola.grups(id);


--
-- TOC entry 2765 (class 2606 OID 134217)
-- Name: qualificacions alumnesunitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk FOREIGN KEY (idalumne) REFERENCES escola.alumnes(idalumne);


--
-- TOC entry 2766 (class 2606 OID 134222)
-- Name: qualificacions alumnesunitatsformatives_fk2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk2 FOREIGN KEY (idunitatformativa) REFERENCES escola.unitatsformatives(id);


--
-- TOC entry 2767 (class 2606 OID 134227)
-- Name: qualificacions alumnesunitatsformatives_fk3; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.qualificacions
    ADD CONSTRAINT alumnesunitatsformatives_fk3 FOREIGN KEY (idprofessor) REFERENCES escola.professor(id);


--
-- TOC entry 2768 (class 2606 OID 134232)
-- Name: grups grups_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_fk FOREIGN KEY (cicle) REFERENCES escola.cicles(id);


--
-- TOC entry 2769 (class 2606 OID 134237)
-- Name: grups grups_fk2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grups
    ADD CONSTRAINT grups_fk2 FOREIGN KEY (tutor) REFERENCES escola.professor(id);


--
-- TOC entry 2770 (class 2606 OID 134242)
-- Name: grupsunitatsformatives grupsunitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk FOREIGN KEY (idaula) REFERENCES escola.aules(id);


--
-- TOC entry 2771 (class 2606 OID 134247)
-- Name: grupsunitatsformatives grupsunitatsformatives_fk3; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk3 FOREIGN KEY (idprofessor) REFERENCES escola.professor(id);


--
-- TOC entry 2772 (class 2606 OID 134252)
-- Name: grupsunitatsformatives grupsunitatsformatives_fk_1; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk_1 FOREIGN KEY (idunitatformativa) REFERENCES escola.unitatsformatives(id);


--
-- TOC entry 2773 (class 2606 OID 134257)
-- Name: grupsunitatsformatives grupsunitatsformatives_fk_2; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.grupsunitatsformatives
    ADD CONSTRAINT grupsunitatsformatives_fk_2 FOREIGN KEY (idgrup) REFERENCES escola.grups(id);


--
-- TOC entry 2774 (class 2606 OID 134262)
-- Name: moduls moduls_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.moduls
    ADD CONSTRAINT moduls_fk FOREIGN KEY (cicle) REFERENCES escola.cicles(id);


--
-- TOC entry 2775 (class 2606 OID 134267)
-- Name: unitatsformatives unitatsformatives_fk; Type: FK CONSTRAINT; Schema: escola; Owner: postgres
--

ALTER TABLE ONLY escola.unitatsformatives
    ADD CONSTRAINT unitatsformatives_fk FOREIGN KEY (idmodul) REFERENCES escola.moduls(id);


-- Completed on 2021-03-20 22:18:38

--
-- PostgreSQL database dump complete
--

